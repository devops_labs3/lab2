FROM alpine:latest AS build

WORKDIR /tmp
RUN apk --no-cache --update add build-base gnupg pcre-dev wget zlib-dev linux-headers libressl-dev zlib-static
RUN set -x && wget -q http://nginx.org/download/nginx-1.23.3.tar.gz && tar -xf nginx-1.23.3.tar.gz

WORKDIR /tmp/nginx-1.23.3
RUN ./configure --with-ld-opt="-static" --with-http_ssl_module && make && make install && strip /usr/local/nginx/sbin/nginx
RUN ln -sf /dev/stdout /usr/local/nginx/logs/access.log && ln -sf /dev/stderr /usr/local/nginx/logs/error.log

FROM scratch

COPY --from=build /usr/local/nginx /usr/local/nginx
COPY --from=build /etc/passwd /etc/group /etc/

EXPOSE 100 200

ENTRYPOINT ["/usr/local/nginx/sbin/nginx"]
CMD ["-g", "daemon off;"]
